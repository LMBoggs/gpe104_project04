﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    //Declare variables
    public float speed; //speed variable to be inherited by all pawns
    public float jumpForce; //jump force variable to be inhertied by all pawns
    public Rigidbody2D rb2d; // var to store rigidbody of pawn
    public bool isGrounded; // var to store if the pawn is grounded, used to calculate max jumps
    public int maxJumps; // designer friendly var to store max jumps before pawn must ground again
    public int jumpCount; // inherited by children, counts jumps since last grounded
    public Animator myAnimator; //inherited by children, hold animator of pawn
    public bool isFlipped;


    // Use this for initialization
    public virtual void Awake () {
		
        //set variables
	    rb2d = GetComponent<Rigidbody2D>(); //load this objects rigidbody into the variable
	    myAnimator = GetComponent<Animator>(); //load this object's Animator into the variable

	}

    // Update is called once per frame
    public virtual void Update () {
		
	}

    public virtual void Move(float xMovement, bool jump)
    {

    }
}

