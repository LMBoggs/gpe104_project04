﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PenguinPawn : Pawn {

    //Declare variables
    //private bool isFlipped;
    [HideInInspector]public bool isHurt; // stores if the player is hurt, set by spikes on collision
    public float hurtHeight; // des. friendly var to hold how high pawn moves when damaged
    public float hurtDistance; // des. friendly var to hold how far the pawn moves when damaged
    private AnimatorStateInfo animStateInfo; // var to glean info about current animation, used in damage anim.
    private AudioSource jumpSound; // var to hold sound when pawn jumps
    public bool UseAsPlayerPawn;

 

	
    // Use this for initialization
	public override void Awake ()
	{
        //Call start function of parent (getting components)
	    base.Awake();
	    isFlipped = false; // pawn is not flipped at game start
    
        //load variables
	    jumpSound = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	public override void Update () {
		
	}


    public override void Move(float xMovement, bool jump)
    {

        //Face correct direction
        //----------------------
        //From right to left
        if (xMovement < 0.0f && isFlipped == false)
        {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            isFlipped = true; 
        }

        //From left to Right
        if (xMovement > 0.0f && isFlipped == true)
        {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            isFlipped = false;
        }
        //--------------------------

        //Damaged animations
        if (isHurt == true)
        {
            //Play animation
            myAnimator.Play ("Damage");

            //Move up slightly
            //transform.position = transform.position + (Vector3.up * hurtHeight);
            rb2d.AddForce(Vector2.up * hurtHeight * Time.deltaTime);

            //Move over slightly
            //If the player is facing right, bump to left
            if (isFlipped == false)
            {
                rb2d.AddForce(Vector2.left * hurtDistance * Time.deltaTime);
            }

            //Else if they are facing left, bump to the right
            else
            {
                rb2d.AddForce(Vector2.right * hurtDistance * Time.deltaTime);
            }

            //Reset isHurt bool
            isHurt = false;
        }


        //Horizontal movement on x axis
        transform.position = transform.position + (Vector3.right * xMovement * speed);
        
        //if not moving but not jumping, play walk animation
        if (xMovement != 0.0f && isGrounded == true && isHurt == false)
        {
            myAnimator.Play ("Walk");
        }

        if (xMovement == 0.0f && isGrounded == true && isHurt == false)
        {
            myAnimator.Play("Idle");
        }

        //Jumping
        if (jump == true)
        {
            //If grounded, jump and increment jumpCount
            if (isGrounded == true && isHurt == false)
            {
                //Jump, play animation, play sound
                rb2d.AddForce(Vector2.up * jumpForce);
                myAnimator.Play("Jump");
                jumpSound.Play();

                //increment jumpCount
                jumpCount++; 

                //set isGrounded to false
                isGrounded = false; 
            }

            //if not grounded, check jumpCount
            else if (isGrounded == false && isHurt == false)
            {
                //If max jumps has not been reached
                if (jumpCount < maxJumps)
                {
                    //Jump, play animation, play sound
                    rb2d.AddForce(Vector2.up * jumpForce);
                    myAnimator.Play("Jump");
                    jumpSound.Play();

                    //increment jumpCount
                    jumpCount++;
                }

            }

        }


        //Landing
        //Note: isGrounded is set to true/false by footTrigger, jumpCount is reset to 0 there also



        /*
        //If player lands, set isGrounded to true and set jumpCount to 0
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 1.8f);
        //HELP: 1.7f is too short, 1.8 is too long for jump ray cast. Need to use double, but raycast will not accept it. 
        if (hitInfo.collider != null)
        {
            //Debug.Log("I landed on a " + hitInfo.collider.gameObject.name);
            isGrounded = true; //player is grounded
            jumpCount = 0; //set jump count to 0
        }
        */

    

    }//end of Move()



}
