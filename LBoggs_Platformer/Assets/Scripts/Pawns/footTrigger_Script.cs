﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footTrigger_Script : MonoBehaviour {

    //Declare variables
    public Pawn parent; //hold parent pawn object
    private int countInTrigger; // counts number of objects in trigger, when empty -> isGrounded is false


    //if something enters footTrigger, count it and set is grounded to true
    void OnTriggerEnter2D(Collider2D other)
    {
        //add object to count on enter
        countInTrigger++; 

        //set isGrounded to true
        parent.isGrounded = true; 

        //set jumpCount to 0
        parent.jumpCount = 0;
    }

    //if nothing is in trigger, remove from count
    void OnTriggerExit2D(Collider2D other)
    {
        //remove object from count
        countInTrigger--;
    }

    void Update()
    {
        //When trigger is empty, isGrounded is false
        if (countInTrigger <= 0)
        {
            parent.isGrounded = false;
        }
        
    }
}
