﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour {

    //Declare variables
    public Pawn pawn; // var to hold pawn for this controller

	// Use this for initialization
	public virtual void Start ()
	{
		//Set pawn to the component on this object
	    pawn = GetComponent<Pawn>();
	}

}
