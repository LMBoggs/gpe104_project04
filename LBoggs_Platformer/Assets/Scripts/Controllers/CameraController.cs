﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //Declare variables
    public Transform pawnTF; //hold transform of player pawn
    public float offsetX; // Designer friendly var to change x camera offset
    public float offsetY; //Designer friendly var to change y camera offset


	// Update is called once per frame
	void LateUpdate () {
		
        //Set camera position to player pawn x position plus offset, maintain y position (do not jump with pawn)
	    transform.position = new Vector3(pawnTF.position.x + offsetX, pawnTF.position.y + offsetY, transform.position.z);


	}
}
