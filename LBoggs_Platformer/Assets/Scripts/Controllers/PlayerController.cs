﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    //declare variables
    public float horizontalMove; //public float to store the x axis of controller
    public bool jump; //bool to pass to pawn for jumping

	// Use this for initialization
	public override void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
        //update the controller horizontal axis
	    horizontalMove = Input.GetAxis("Horizontal");

        //If player presses Jump, set to true
	    if (Input.GetButtonDown("Jump"))
	    {
	        jump = true;
	    }

    }
    
    void FixedUpdate()
    {
        //Movement called in Update, occurs in FixedUpdate
        //----------------------------------------------------
        //Call Pawn move function
        pawn.Move(horizontalMove * Time.fixedDeltaTime, jump);
        jump = false; // set jump to false after calling move function
    }
}
