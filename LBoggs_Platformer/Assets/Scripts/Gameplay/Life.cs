﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{

    [HideInInspector] Animator myAnimator; 


	// Use this for initialization
	void Start () {
		
        //set my animator to the variable
	    myAnimator = GetComponent<Animator>();

	    
	    //Player starts with max lives, all turn on
        myAnimator.Play("life_ON");

	}

    public void SetLife(bool life)
    {
        if (life)
        {
            myAnimator.Play("life_ON");
        }

        else
        {
            myAnimator.Play("life_OFF");
        }
    }
}
