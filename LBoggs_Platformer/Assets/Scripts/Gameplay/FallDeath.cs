﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallDeath : MonoBehaviour {

    //Declare variables
    private AudioSource mySound;

    void Start()
    {
        //Set variables
        mySound = GetComponent<AudioSource>(); //load this object's audio source to var
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //If the penguin hit the fall zone
        if (other.gameObject.GetComponent<PenguinPawn>() != null)
        {
            //disable the player controller
            GameManager.instance.Player.enabled = false;

            //play "fall" (jump) animation on penguin
            other.gameObject.GetComponent<PenguinPawn>().myAnimator.Play("Jump");

            //Stop level music
            AudioManager.instance.StopSceneMusic();

            //play death sound
            mySound.Play();

            //Decrease player lives in game manager
            GameManager.instance.lives--;

            //when death sound is finished, reset penguin to last checkpoint via GameManager if player has lives
            if (GameManager.instance.lives > 0)
            {
                Invoke("InvokeGM_Checkpoint", mySound.clip.length);
            }

            //else player has no lives left, finish death sound and load game over scene
            else
            {
                Invoke("Invoke_GameOver", mySound.clip.length);
            }
        }

    }


    void OnTriggerExit2D(Collider2D other)
    {
        //If the penguin hit the fall zone
        if (other.gameObject.GetComponent<PenguinPawn>() != null)
        {

            //play "fall" (jump) animation on penguin
            other.gameObject.GetComponent<PenguinPawn>().myAnimator.Play("Jump");


        }
    }
    void InvokeGM_Checkpoint()
    {
        GameManager.instance.LoadCheckpoint();
    }


    void Invoke_GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

}
