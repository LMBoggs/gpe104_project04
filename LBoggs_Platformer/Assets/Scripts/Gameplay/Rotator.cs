﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    //Declare variables
    public float degreesToRotate = 90; // des. friendly float to modify degrees rotated per frame

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{

	    //transform.eulerAngles = Vector3.forward * degreesToRotate;

        transform.Rotate(new Vector3(0, 0, degreesToRotate));

    }
}
