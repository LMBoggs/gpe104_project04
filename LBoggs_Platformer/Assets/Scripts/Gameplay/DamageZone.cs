﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{

    private AudioSource mySound;

    void Start()
    {
        //Load variables
        mySound = GetComponent<AudioSource>();
    }


    //When a pawn collides with a penguin pawn
    private void OnCollisionEnter2D(Collision2D other)
    {
        //if the collision is on a penguin pawn
        if (other.gameObject.GetComponent<PenguinPawn>())
        {
            //Set ishurt bool on pawn to true
            other.gameObject.GetComponent<PenguinPawn>().isHurt = true;

            //Play the sound
            mySound.Play();

        }
        
        

    }
}
