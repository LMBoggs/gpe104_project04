﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollectItem : MonoBehaviour {

    //Declare variables
    public AudioSource mySound; //enter audio source from Audio Manager child object in inspector

    public enum ItemType
    {
        StarCoin,
        LifeCoin,
        SpeedCoin,
        InvincibleCoin
    };

    public ItemType pickUp; //designer friendly dropdown to identify item type

    public TMP_Text StarCounterText; // load star counter GUI text
    public static int totalStarCoins; // var to hold total star coin count

    void Awake()
    {
        //Reset starCount
        totalStarCoins = 0;
    }
    void Start()
    {
        //if type is StarCoin, add to count and display in GUI
        if (pickUp == ItemType.StarCoin)
        {
            totalStarCoins++;
            StarCounterText.SetText(GameManager.instance.starCoins.ToString() + " / " + totalStarCoins.ToString());
        }
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        //Play sound on audio manager
        AudioManager.instance.PlayPickup(mySound);

        //If item is star coin, add to collected count
        if (pickUp == ItemType.StarCoin)
        {
            GameManager.instance.starCoins++;
            StarCounterText.SetText(GameManager.instance.starCoins.ToString() + " / " + totalStarCoins.ToString());
        }








        //Destroy this object
        Destroy(this.gameObject);
    }


}
