﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeInvisible : MonoBehaviour {

    //Declare variables
    private SpriteRenderer myRenderer; // var to hold sprite renderer of this object

	// Use this for initialization
	void Start () {
		
        //Load variables
	    myRenderer = GetComponent<SpriteRenderer>(); // load the sprite renderer on this object to the variable

        //Disable sprite renderer on start
	    myRenderer.enabled = false; 
	}

}
