﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Victory : MonoBehaviour {

    //Declare variables
    private AudioSource mySound;

	// Use this for initialization
	void Start () {
		
        //Load variables
	    mySound = GetComponent<AudioSource>();

	}

    void OnTriggerEnter2D(Collider2D other)
    {
        //If the penguin pawn entered the collider
        if (other.gameObject.GetComponent<PenguinPawn>())
        {
            //Stop level music
            AudioManager.instance.StopSceneMusic();

            //When our sound concludes, load victory screen
            Invoke("InvokeVictory", mySound.clip.length);

        }
    }

    void InvokeVictory()
    {
        SceneManager.LoadScene("Victory");
    }
}
