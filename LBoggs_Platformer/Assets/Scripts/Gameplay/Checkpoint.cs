﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

    //Declare variables
    [HideInInspector] public Animator myAnimator;
    private bool isChecked;
    private AudioSource mySound; 


    void Start()
    {
        //Set variables
        myAnimator = GetComponent<Animator>(); //load this object's animator to the var
        mySound = GetComponent<AudioSource>(); //load this object's audio source to the var

        isChecked = false; // the checkpoint starts the game unchecked
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        //if the penguin pawn collides with the checkpoint
        if (other.gameObject.GetComponent<PenguinPawn>() != null)
        {
            //if the player has not already triggered this checkpoint
            if (isChecked == false)
            {
                //Set player checkpoint to this checkpoint
                GameManager.instance.SetCheckpoint(transform.position);
                isChecked = true; //this checkpoint is checked

                //Feedback loop
                myAnimator.Play("checkpoint_ON"); //play ON animation
                mySound.Play(); //play checkpoint sound

            }
        }
    }






}
