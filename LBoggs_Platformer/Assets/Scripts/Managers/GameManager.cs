﻿using System.Collections;
using TMPro;
//using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //Declare Variables
    public static GameManager instance; // singleton for this GameManager
    public PlayerController Player; //var to store player controller and access variables
    public Camera mainCam; // var to store the main camera, used for playClipAtPoint objects, like coins
    [HideInInspector]public Vector3 Checkpoint; // var to store player's current checkpoint (set by hitting checkpoints)
    public int lives = 3; // variable to hold player lives
    [HideInInspector] public int starCoins = 0; //var to hold the player's collected star coins

    void Awake()
    {
        //If another GameManager does not exist
        if (instance == null)
        {
            //Load this object as singleton pattern
            instance = this;

            //Do no destroy this game object on load
            //DontDestroyOnLoad(gameObject); 
        }

        //Else, we already have a GameManager
        else
        {
            //Destroy the newbie GameManager
            Destroy(this.gameObject);
        
            //Reveal error in console
            Debug.Log("Warning: A second game manager was detected and destroyed");
        }
        
    }


    public void SetCheckpoint(Vector3 newCP)
    {
        Checkpoint = newCP;
    }

    public void LoadCheckpoint()
    {
        //Set player's pawn to last checkpoint location
        Player.pawn.transform.position = Checkpoint; 

        //Face pawn to the right
        if (Player.pawn.isFlipped)
        {
            Player.pawn.transform.localScale = new Vector3(-Player.pawn.transform.localScale.x, Player.pawn.transform.localScale.y, Player.pawn.transform.localScale.z);
            Player.pawn.isFlipped = false;
        }

        //Restart scene music
        AudioManager.instance.PlaySceneMusic();

        //Re-enable player controller
        Player.enabled = true;

    }

}
