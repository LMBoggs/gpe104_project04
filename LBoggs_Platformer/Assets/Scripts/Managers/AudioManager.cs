﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    //Declare variables
    public static AudioManager instance; // singleton var for audio manager

    private AudioSource myAudioSource; // var to store audio source on this object
    public AudioClip sceneMusic; //designer friendly var to hold level music clip

    void Awake()
    {
        //If another AudioManager does not exist
        if (instance == null)
        {
            //Load this object as singleton pattern
            instance = this;

            //Do no destroy this game object on load
            //DontDestroyOnLoad(gameObject);
        }

        //Else, we already have a AduioManager
        else
        {
            //Destroy the newbie AudioManager
            Destroy(this.gameObject);

            //Reveal error in console
            Debug.Log("Warning: A second audio manager was detected and destroyed");
        }

    }

    void Start()
    {
        //Set variables
        myAudioSource = GetComponent<AudioSource>(); //load audio source on this object to variable

        myAudioSource.clip = sceneMusic;//load clip with scene music variable
        
        
        //Play SceneMusic on start
        PlaySceneMusic();
    }

    //Sets new clip to audio source
    public void SetSceneMusic(AudioClip newMusic)
    {
        //Load sceneMusic var with newMusic passed in
        sceneMusic = newMusic;
        
        //Set audio source clip to new info
        myAudioSource.clip = sceneMusic;
    }

    public void PlaySceneMusic()
    {
        myAudioSource.Play();
    }

    public void PauseSceneMusic()
    {
        myAudioSource.Pause();
    }


    public void StopSceneMusic()
    {
        myAudioSource.Stop();
    }

    //Plays a OneShot of any clip in any audio source, passed into the function
    public void PlayPickup(AudioSource sound)
    {
        sound.PlayOneShot(sound.clip);
    }



}
