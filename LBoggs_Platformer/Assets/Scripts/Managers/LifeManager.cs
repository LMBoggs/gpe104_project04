﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{

    public Life Life_01;
    public Life Life_02;
    public Life Life_03; 


	// Update is called once per frame
	void Update () {

	    if (GameManager.instance.lives >= 3)
	    {
            Life_01.SetLife(true);
            Life_02.SetLife(true);
            Life_03.SetLife(true);
	    }

        else if (GameManager.instance.lives == 2)
	    {
            Life_01.SetLife(true);
            Life_02.SetLife(true);
            Life_03.SetLife(false);
	    }

        else if (GameManager.instance.lives == 1)
	    {
            Life_01.SetLife(true);
            Life_02.SetLife(false);
            Life_03.SetLife(false);
	    }

        else if (GameManager.instance.lives <= 0)
	    {
            Life_01.SetLife(false);
            Life_02.SetLife(false);
            Life_03.SetLife(false);
	    }


	}
}
